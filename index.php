<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <title>Vacunación</title>
</head>

<body>


  <div class="container">
    <div class="row my-5">
      <div class="col text-center">
        <h1>Vacunación</h1>
      </div>
    </div>
    <form action="" method="POST">
      <div class="row">

        <div>
          <label class="form-label" for="identificacion">Identificación</label>
          <input class="form-control" type="text" name="identificacion" id="identificacion" required />

        </div>
        <div>
          <label class="form-label" for="nombres">Nombres</label>
          <input class="form-control" type="text" name="nombres" id="nombres" required />
        </div>
        <div>
          <label class="form-label" for="apellidos">Apellidos</label>
          <input class="form-control" type="text" name="apellidos" id="apellidos" required />
        </div>
        <div>
          <label class="form-label" for="tipoDeBiologico">Tipo de biológico</label>
          <select class="form-control" name="tipoDeBiologico" id="tipoDeBiologico" required>
            <option value="Pfizer">Pfizer</option>
            <option value="Moderna">Moderna</option>
            <option value="Janssen">Janssen</option>
            <option value="Astrazeneca">Astrazeneca</option>
          </select>

          <!-- <input class="form-control" type="text" name="tipoDeBiologico" id="tipoDeBiologico" required /> -->
        </div>
        <div>
          <label class="form-label" for="fechaPrimeraDosis">Fecha primera dosis</label>
          <input class="form-control" type="date" name="fechaPrimeraDosis" id="fechaPrimeraDosis" required />
        </div>
        <div>
          <label class="form-label" for="fechaSegundaDosis">Fecha de segunda dosis</label>
          <input class="form-control" type="date" name="fechaSegundaDosis" id="fechaSegundaDosis" required />
        </div>
        <div class="mt-3">
          <input class="btn btn-primary" type="submit" value="Enviar" />
        </div>
      </div>
    </form>
    <?php
    error_reporting(E_ERROR | E_PARSE);
    $identificacion = $_POST["identificacion"];
    $nombres = $_POST["nombres"];
    $apellidos = $_POST["apellidos"];
    $tipoDeBiologico = $_POST["tipoDeBiologico"];
    $fechaPrimeraDosis = $_POST["fechaPrimeraDosis"];
    $fechaSegundaDosis = $_POST["fechaSegundaDosis"];
    $filename = "db.txt";
    if (!empty($identificacion) && !empty($nombres) && !empty($apellidos) && !empty($tipoDeBiologico) && !empty($fechaPrimeraDosis) && !empty($fechaSegundaDosis)) {
      $data = $identificacion . ";" . $nombres . ";" . $apellidos . ";" . $tipoDeBiologico . ";" . $fechaPrimeraDosis . ";" . $fechaSegundaDosis . "\n";
      $fp = fopen($filename, "a+");
      fwrite($fp, $data);
      fclose($fp);
    }
    ?>

    <div class="row mt-3">
      <div class="col">
        <table class="table">
          <thead>

            <tr>
              <td>Identificación</td>
              <td>Nombres</td>
              <td>Apellidos</td>
              <td>Tipo de biologico</td>
              <td>Fecha de primera dosis</td>
              <td>Fecha de segunda dosis</td>
            </tr>
          </thead>
          <tbody>

            <?php
            $filename = "db.txt";
            if ($file = fopen($filename, "r")) {
              while (!feof($file)) {
                $line = fgets($file);
                $persona = explode(";", $line);
                echo "<tr>";
                foreach ($persona as $dato) {
                  echo "<td>$dato</td>";
                }
                echo "</tr>";
              }
              fclose($file);
            }
            ?>
          </tbody>
        </table>
      </div>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>

</html>